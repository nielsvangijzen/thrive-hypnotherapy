FROM php:8-apache

WORKDIR /srv

ADD .docker/apache.conf /etc/apache2/sites-available/000-default.conf

RUN a2enmod rewrite

COPY public ./public
COPY ./php ./php
